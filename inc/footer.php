<?php 
$timerStop = microtime(true);
$timeSpent= $timerStop - $timerStart;
?>

<footer class="footer">
	<span class="navbar-logo pull-right"><a href="https://github.com/wikimedia-france/supported-by-wmfr-stats"><img title="Developed by Sylvain Boissel" src="img/github-light.png" /></a></span>
	<div class="container">
		<p class="text-muted">Script runtime: <?php echo round($timeSpent,2); ?> seconds.</p>
	</div>      		
</footer>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="//tools-static.wmflabs.org/cdnjs/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
<script src="//tools-static.wmflabs.org/cdnjs/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js"></script>
<script>
$(function() {
	$('#category').selectize({
		create: true,
		sortField: 'text',
		maxItems: 1
	});
});
</script>
  </body>
</html>